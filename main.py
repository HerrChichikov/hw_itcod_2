class Square:
    def __init__(self, side_a: int, side_b: int):
        self.side_a = side_a
        self.side_b = side_b

    def area(self):
        return self.side_a * self.side_b
